MLOps3. Spring 2024
==============================
## Project description

In this project we'll predict a full year worth of sales for various fictitious learning modules from different fictitious Kaggle-branded stores in different countries.

Dataset was taken from Kaggle:
https://www.kaggle.com/competitions/playground-series-s3e19

This dataset is completely synthetic, but contains many effects of real-world data, e.g., weekend and holiday effect, seasonality, etc.

The task of this project is to predict sales during for year 2022


## How to reproduce the project

Clone this repo into your local machine with the command:
`git clone https://gitlab.com/avisprof/mlflow_homework.git`

Install dependencies from the `pytproject.toml` with the commands:
`pip install poetry`
`poetry install` 

Install MLFlow server:
`pip install mlflow`

Run MLFLow server locally with the command:
`mlflow server --host 127.0.0.1 --port 8080`

![start_mlflow_server](reports/figures/00_start_mlflow_server.png)

## MLFlow Exepriments

In this [work](notebooks/05_mlflow.ipynb) I train and track some differents model to predict feature sales and compare it results: 

![mlflow_experiments](reports/figures/01_mlflow_experiments.png)


The best of the three models is the `LGBMRegressor`:
![mlflow_metrics_chart](reports/figures/02_mlflow_metrics_chart.png)

Also I save parameters and metric of trained models:
![mlflow_params_metrics](reports/figures/03_mlflow_params_metrics.png)

And I save artifacts of trained models:
![mlflow_artifacts](reports/figures/04_mlflow_artifacts.png)

