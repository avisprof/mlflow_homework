from pathlib import Path

import click
import omegaconf
import pandas as pd

@click.command()
@click.argument('input_file', type=click.Path(exists=True))
@click.argument('output_file', type=click.Path())
def cli_preprocess_data(input_file: Path, output_file: Path):

    print(f'Read input file: {input_file}')
    data = pd.read_csv(input_file, index_col=0, parse_dates=['date'])

    cfg = omegaconf.OmegaConf.load('conf/config.yaml')
    params = cfg["preprocess"]
    covid_start_date = params.covid_start_date
    covid_end_date = params.covid_end_date
    print(f'Preprocess: remove COVID data from {covid_start_date} to {covid_end_date}...')
    data_clean = data.query('date < @covid_start_date or date > @covid_end_date').copy()

    print(f"Save results to file: {output_file}")
    data_clean.to_csv(output_file, compression='zip')

if __name__ == '__main__':
    cli_preprocess_data()
