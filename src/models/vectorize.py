import pickle
from pathlib import Path

import click
import omegaconf
import pandas as pd

from sklearn.preprocessing import LabelEncoder

@click.command()
@click.argument('input_file', type=click.Path(exists=True))
@click.argument('output_file', type=click.Path())
def cli_train_vectorize(input_file: Path, output_file: Path):

    print(f'Read input file: {input_file}')
    data = pd.read_csv(input_file, index_col=0, parse_dates=['date'])

    cfg = omegaconf.OmegaConf.load('conf/config.yaml')
    params = cfg["vectorizer"]
    cat_columns = params["cat_columns"]

    print(f"Vectorize categorical columns: {cat_columns}")

    encoders = {}
    for col in cat_columns:
        enc = LabelEncoder()
        enc.fit(data[col])
        encoders[col] = enc

    print(f"Save encoder to file: {output_file}")
    with open(output_file, "wb") as f_out:
        pickle.dump(encoders, f_out)

if __name__ == '__main__':
    cli_train_vectorize()


