import pickle
from pathlib import Path

import click
import omegaconf
import pandas as pd
import mlflow

from sklearn.ensemble import RandomForestRegressor

@click.command()
@click.argument('input_file', type=click.Path(exists=True))
@click.argument("output_file_model", type=click.Path())
def cli_train_model(input_file: Path, output_file_model: Path):

    print(f'Read input file: {input_file}')
    X = pd.read_csv(input_file, index_col=0)
    y = X.pop('num_sold').values

    cfg = omegaconf.OmegaConf.load('conf/config.yaml')
    params_mlflow = cfg["mlflow"]
    mlflow.set_tracking_uri(params_mlflow["tracking_uri"])
    experiment = mlflow.set_experiment(params_mlflow["experiment_name"])

    params = cfg["models"]
    model_params = params["random_forest"]

    with mlflow.start_run(run_name="RandomForest") as run:
        model = RandomForestRegressor(**model_params)
        model_name = model.__class__.__name__
	
        print(f"Train model {model_name} with params {model_params}")
        model.fit(X, y)

        print(f'Save {model_name} to file: {output_file_model}')
        with open(output_file_model, 'wb') as f_out:
            pickle.dump(model, f_out)


if __name__ == '__main__':
    cli_train_model()

