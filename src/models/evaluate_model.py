import pickle
from pathlib import Path

import click
import pandas as pd
import json

from sklearn.metrics import mean_absolute_error

@click.command()
@click.argument("input_data", type=click.Path(exists=True))
@click.argument("input_model", type=click.Path(exists=True))
@click.argument("output_metric", type=click.Path())
def cli_evaluate_model(input_data: Path, input_model: Path, output_metric: Path):
    
    print(f"Read input file: {input_data}")
    X = pd.read_csv(input_data, index_col=0)
    y = X.pop('num_sold').values

    print(f"Load model: {input_model}")
    with open(input_model, "rb") as f_in:
        model = pickle.load(f_in)

    print(f"Make predictions ...")
    y_pred = model.predict(X)

    score = mean_absolute_error(y, y_pred)
    model_name = model.__class__.__name__
    print(f"Evaluate {model_name} by metric MAE: {score:.3f}")

    report = {"MAE": score}

    print(f"Save metrics to file: {output_metric}")
    with open(output_metric, "w") as f_out:
        json.dump(report, f_out, indent=2)

if __name__ == '__main__':
    cli_evaluate_model()

