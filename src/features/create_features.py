from pathlib import Path
import datetime as dt

import click
import numpy as np
import pandas as pd

@click.command()
@click.argument('input_file', type=click.Path(exists=True))
@click.argument('output_file', type=click.Path())
def cli_create_features(input_file: Path, output_file: Path):

    print(f'Read input file: {input_file}')
    data = pd.read_csv(input_file, index_col=0, parse_dates=['date'])
	
    print('Create features...')
    data['year'] = data['date'].dt.year - 2017
    data['month'] = data['date'].dt.month
    data['quarter'] = data['date'].dt.quarter
    data['day'] = data['date'].dt.day
    data['weekday'] = data['date'].dt.weekday
    data['dayofyear'] = data['date'].dt.dayofyear

    data['time_no'] = (data['date'] - dt.datetime(2017, 1, 1)) // dt.timedelta(days=1)
    data['year_sin_1'] = np.sin(np.pi * data['time_no'] / 182.5)
    data['year_cos_1'] = np.cos(np.pi * data['time_no'] / 182.5)
    data['year_sin_0.5'] = np.sin(np.pi * data['time_no'] / 365.0)
    data['year_cos_0.5'] = np.cos(np.pi * data['time_no'] / 365.0)

    del data['date']
    
    print(f"Save data to file: {output_file}")
    data.to_csv(output_file, compression='zip')

if __name__ == '__main__':
    cli_create_features()

