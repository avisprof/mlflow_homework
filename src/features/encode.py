import pickle
from pathlib import Path

import click
import omegaconf
import pandas as pd

@click.command()
@click.argument("input_file", type=click.Path(exists=True))
@click.argument("input_file_vect", type=click.Path(exists=True))
@click.argument("output_file", type=click.Path())
def cli_encode_data(input_file: Path, input_file_vect: Path, output_file: Path):

    print(f'Read input file: {input_file}')
    data = pd.read_csv(input_file, index_col=0)

    print(f"Read vectorizer: {input_file_vect}")
    with open(input_file_vect, "rb") as f_in:
        encoders = pickle.load(f_in)

    cfg = omegaconf.OmegaConf.load('conf/config.yaml')
    params = cfg["vectorizer"]
    cat_columns = params["cat_columns"]
 
    print(f"Encode data with fitted encoder...")
    for col in cat_columns:
        print(f"\tColumn: {col}")
        enc = encoders[col]
        data[col] = enc.transform(data[col])
    
    print(f"Save data to file: {output_file}")
    data.to_csv(output_file)

if __name__ == '__main__':
    cli_encode_data()





